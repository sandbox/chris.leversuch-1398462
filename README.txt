=======================
Amazon S3 Logs
=======================
Richard Peacock (richard@richardpeacock.com)

This module reads and parses your Amazon S3 account's various
log files, so you can easily track your most popular uri's,
and how much bandwidth has been used per-file.  It also makes sure
to never download the same log file twice (saving you bandwidth),
and can be configured to only check for new log files every few hours
(saving you GET and LIST requests).

Requires: the Amazon S3 module (http://drupal.org/project/amazon_s3)
Also requires that you have set up a special log bucket as per
Amazon's instructions:
http://docs.amazonwebservices.com/AmazonS3/2006-03-01/LoggingHowTo.html
I know that may seem hard or anoying, but it honestly only takes about
five minutes.

Special note: If any developers want to work on a way to automate the process of
creating a log bucket, I would greatly appreciate it!  Please post your thoughts
or code in an issue on http://drupal.org/project/amazon_s3_logs.

===============
Directions
===============
- Install the required amazon_s3 module, and set up a special log bucket on your
  Amazon S3 account (follow the directions in the link above).
- Install this module at /modules/amazon_s3_logs and enable it.
- Visit admin/settings/amazon-s3-logs to configure.
- Wait for log files to appear in your log bucket, and make sure your cron is
  running correctly.
- View your usage statistics at: Administer -> Reports -> Amazon S3 Logs or at
  admin/reports/amazon-s3-logs

===============
Sample log entry
===============

All the items below appear on the same line, space-delimited.

56770a6b24818eb8843855908fdaa99f5c0436962101abf59929e3b4f5e2d1b9
bytebyte.net
[18/Jul/2010:12:34:27 +0000]
75.66.148.207
56770a6b24818eb8843855908fdaa99f5c0436962101abf59929e3b4f5e2d1b9
2D865962988393F3
REST.GET.OBJECT
WordHunt.jar
"GET /WordHunt.jar?AWSAccessKeyId=AKIAIW6QDPT62VSFSXQQ&Expires=1279542847&Signature=O4QRspoxdGigDfrEuuSHfjY9%2B3o%3D HTTP/1.1"
304
-
-
8344
15
-
"-"
"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3"
-
